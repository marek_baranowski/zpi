name := "zpi"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  "mysql" % "mysql-connector-java" % "5.1.8",
  javaJdbc,
  javaEbean,
  cache
)     

play.Project.playJavaSettings
