import play.*;
import play.data.format.Formatters;
import play.libs.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import com.avaje.ebean.*;

import controllers.routes;
import models.*;

public class Global extends GlobalSettings {

	public void onStart(Application app) {
		InitialData.insert(app);
	}

	static class InitialData {

		public static void insert(Application app) {
			Picture pic = Picture.find.byId(1);
			if (pic == null) {
				pic = new Picture();
				File file = new File("public/images/default-avatar.png");
				try {
					pic.picture = Files.readAllBytes(file.toPath());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				pic.dateUploaded = new Timestamp(System.currentTimeMillis());
				pic.save();
			}
			if (User.find.where().eq("email", "mbrnwsk@gmail.com").findUnique() == null) {
				User user1 = new User();
				user1.email = "mbrnwsk@gmail.com";
				user1.password = "testest1";
				user1.userName = "mbrnwsk";
				user1.firstName = "Marek";
				user1.lastName = "Baranowski";
				user1.profilePicture = pic;
				user1.save();
			}
			if (User.find.where().eq("email", "baranowski.mt@gmail.com").findUnique() == null) {
				User user2 = new User();
				user2.email = "baranowski.mt@gmail.com";
				user2.password = "testest1";
				user2.userName = "baranOwski";
				user2.firstName = "Marek";
				user2.lastName = "Kowalski";
				user2.profilePicture = pic;
				user2.save();
			}
			Formatters.register(Date.class,
					new Formatters.SimpleFormatter<Date>() {
						@Override
						public Date parse(String input, Locale l)
								throws ParseException {
							return input.isEmpty() ? null : Date.valueOf(input);
						}

						@Override
						public String print(Date date, Locale l) {
							return date == null ? "" : date.toString();
						}
					});
			Formatters.register(Time.class,
					new Formatters.SimpleFormatter<Time>() {
						@Override
						public Time parse(String input, Locale l)
								throws ParseException {
							if (input.isEmpty()) {
								return null;
							}
							String[] splitted = input.split(":");
							if (splitted.length == 2) {
								input.concat(":00");
							}
							return Time.valueOf(input);
						}

						@Override
						public String print(Time time, Locale l) {
							return time == null ? "" : time.toString();
						}
					});
			Formatters.register(Timestamp.class,
					new Formatters.SimpleFormatter<Timestamp>() {
						@Override
						public Timestamp parse(String input, Locale l)
								throws ParseException {
							return input.isEmpty() ? null : Timestamp
									.valueOf(input);
						}

						@Override
						public String print(Timestamp date, Locale l) {
							return date == null ? "" : date.toString();
						}
					});
		}

	}

}