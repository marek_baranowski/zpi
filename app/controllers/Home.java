package controllers;

import java.sql.Date;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import models.Invitation;
import models.Notification;
import models.Trip;
import models.TripInvitation;
import models.User;
import play.mvc.*;
import views.html.*;

@Security.Authenticated(Secured.class)
public class Home extends Controller {

	public static Result index() {
		User user = User.find.byId(Integer.valueOf(request().username()));
		List<Trip> recentTrips = Trip.find.where().ne("creator", user)
				.eq("visibility", "public").findList();
		return ok(home.render("Home", user,
				Trip.find.where().eq("creator", user).findList(),
				friendsTrips(user.id), recentTrips));
	}

	public static List<Trip> getNextTrip(int userId) {
		return new LinkedList<Trip>();
	}

	public static List<Trip> friendsTrips(int userId) {

		return new LinkedList<Trip>();
	}

	public static Result notifications() {
		User user = User.find.byId(Integer.valueOf(request().username()));
		List<TripInvitation> ti = TripInvitation.find.where()
				.eq("receiver", user).findList();
		List<Invitation> ii = Invitation.find.where().eq("receiver", user)
				.findList();
		List<Notification> l = new LinkedList<Notification>();
		for (TripInvitation zz : ti) {
			Notification n = new Notification();
			n.what = "trip";
			n.sender = zz.sender;
			n.date = zz.date;
			n.trip_ = zz.trip;
			l.add(n);
		}
		for (Invitation zz : ii) {
			Notification n = new Notification();
			n.what = "friend";
			n.sender = zz.sender;
			n.date = zz.date;
			l.add(n);
		}
		Collections.sort(l);
		return ok(notifications.render("Notifications", user, l));
	}
}
