package controllers;

import static play.data.Form.form;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.Lists;

import models.Friendship;
import models.Invitation;
import models.Message;
import models.Picture;
import models.Trip;
import models.User;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.Json;
import play.mvc.*;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import views.html.*;
import views.html.profile.*;
import views.html.trip.*;

@Security.Authenticated(Secured.class)
public class Profile extends Controller {

	public static Result myProfile(Integer id) {
		User visitor = User.find.byId(Integer.valueOf(request().username()));
		User viewed = User.find.byId(id);

		if (visitor.id == viewed.id) {
			List<Picture> psc = Picture.find.where().eq("user.id", viewed.id)
					.findList();
			System.out.println(psc.size());
			return ok(profileAbout.render("My Profile", visitor, visitor, null,
					psc, Form.form(Message.class)));
		}

		String friendshipStatus = "Not friends";
		Friendship friends = Friendship.find.where().eq("user1.id", visitor.id)
				.eq("user2.id", viewed.id).findUnique();
		if (friends != null) {
			friendshipStatus = "Friends";
		} else {
			Invitation inv1 = Invitation.find.where()
					.eq("sender.id", visitor.id).eq("receiver.id", viewed.id)
					.findUnique();
			if (inv1 != null) {
				friendshipStatus = "Waiting for friendship confirmation";
			} else {
				Invitation inv2 = Invitation.find.where()
						.eq("sender.id", viewed.id)
						.eq("receiver.id", visitor.id).findUnique();
				if (inv2 != null) {
					friendshipStatus = "Confirm friendship";
				}
			}
		}

		return ok(profileAbout.render("My Profile", viewed, visitor,
				friendshipStatus, Picture.find.where().eq("user.id", viewed.id)
						.findList(), Form.form(Message.class)));
	}

	public static Result sendInvitation(Integer id) {
		User receiver = User.find.byId(id);
		User sender = User.find.byId(Integer.valueOf(request().username()));

		Invitation invitation = new Invitation();
		invitation.receiver = receiver;
		invitation.sender = sender;
		invitation.date = new Timestamp(System.currentTimeMillis());
		invitation.save();	
		if (Application.members.containsKey(receiver.id)) {
			ObjectNode event = Json.newObject();
			event.put("msg", "friend");
			Application.members.get(receiver.id).write(event);
		}
		return redirect(routes.Profile.myProfile(id));
	}

	public static Result confirmFriendship(Integer id) {
		User receiver = User.find.byId(Integer.valueOf(request().username()));
		User sender = User.find.byId(id);

		Invitation inv = Invitation.find.where().eq("sender.id", sender.id)
				.eq("receiver.id", receiver.id).findUnique();
		if (inv != null) {
			inv.delete();
			Friendship f = new Friendship();
			f.user1 = receiver;
			f.user2 = sender;
			f.save();
			f = new Friendship();
			f.user1 = sender;
			f.user2 = receiver;
			f.save();
		}
		return redirect(routes.Profile.myProfile(id));
	}

	public static Result denyFriendship(int senderId) {
		User receiver = User.find.byId(Integer.valueOf(request().username()));
		User sender = User.find.byId(senderId);
		Invitation.find.where().eq("sender.id", sender.id)
				.eq("receiver.id", receiver.id).findUnique().delete();
		return redirect(routes.Profile.myProfile(senderId));
	}

	public static Result editProfile(int id) {
		User u = User.find.byId(Integer.valueOf(request().username()));
		return ok(editProfile.render("User profile", u));
	}

	public static Result updateProfile() {
		User user = User.find.byId(Integer.valueOf(request().username()));
		Form<User> destForm = form(User.class).bindFromRequest();
		if (destForm.hasErrors()) {
			return badRequest(editProfile
					.render("User profile", destForm.get()));
		} else {
			User newUser = destForm.get();
			newUser.id = user.id;
			newUser.friends = user.friends;
			newUser.profilePicture = user.profilePicture;
			newUser.update();
			return redirect(routes.Profile.myProfile(user.id));
		}
	}

	public static Result pictures(Integer id) {
		User viewed = User.find.byId(id);
		User visitor = User.find.byId(Integer.valueOf(request().username()));
		List<Picture> pictures = Picture.find.where().eq("user.id", id)
				.findList();
		return ok(profilePictures.render("Pictures", viewed, visitor, pictures));
	}

	public static Result renderPhoto(Integer id) {
		Picture pic = Picture.find.byId(id);
		return ok(pic.picture);
	}

	public static Result changeProfilePic(Integer id) {
		User viewed = User.find.byId(id);
		DynamicForm form = DynamicForm.form().bindFromRequest();
		String picId = form.get("picId");
		if (picId.isEmpty()) {
			viewed.profilePicture = Picture.find.byId(1);
		} else
			viewed.profilePicture = Picture.find.byId(Integer.valueOf(picId));
		viewed.update();
		return redirect(routes.Profile.myProfile(viewed.id));
	}

	public static Result upload() {
		User user = User.find.byId(Integer.valueOf(request().username()));
		MultipartFormData body = request().body().asMultipartFormData();
		FilePart picture = body.getFile("picture");
		if (picture != null) {
			Picture pic = new Picture();
			pic.user = user;
			File file = picture.getFile();
			try {
				pic.picture = Files.readAllBytes(file.toPath());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			pic.save();
			if (Picture.find.findRowCount() == 1) {
				user.profilePicture = pic;
				user.update();
			}
			return redirect(routes.Profile.pictures(user.id));
		} else {
			return redirect(routes.Profile.pictures(user.id));
		}
	}

	public static Result friends(Integer id) {
		User viewed = User.find.byId(id);
		User visitor = User.find.byId(Integer.valueOf(request().username()));
		List<User> friends = new LinkedList<User>();
		List<Friendship> friendships = Friendship.find.where()
				.eq("user1.id", id).findList();
		for (Friendship f : friendships) {
			friends.add(f.user2);
		}
		return ok(profileFriends.render("Pictures", viewed, visitor, friends));
	}

	public static Result userTrips(Integer id) {
		User viewed = User.find.byId(id);
		User visitor = User.find.byId(Integer.valueOf(request().username()));
		return ok(profileTrips.render("Pictures", viewed, visitor, Trip.find
				.where().eq("users.id", viewed.id).findList()));
	}

	public static Result sendMessage(String flag) {
		Form<Message> messageForm = Form.form(Message.class).bindFromRequest();
		Message m = messageForm.get();
		User sender = User.find.byId(Integer.valueOf(request().username()));
		User receiver = User.find.byId(m.receiverId);
		m.sender = sender;
		m.receiver = receiver;
		m.date = new Timestamp(System.currentTimeMillis());
		m.status = "Unread";
		m.save();
		if (Application.members.containsKey(receiver.id)) {
			ObjectNode event = Json.newObject();
			event.put("msg", "msg");
			Application.members.get(receiver.id).write(event);
		}
		if (flag.equals("inbox")) {
			return redirect(routes.Application.inbox());
		}
		return redirect(routes.Profile.myProfile(receiver.id));
	}

	public static Result peopleSearch(String phrase) {
		User user = User.find.byId(Integer.valueOf(request().username()));
		String sql = "select user.id, first_name, last_name, user_name, date_of_birth, gender, hometown, email, phone, about, date_registered form"
				+ "from user join picture on user.id";
		List<User> people = User.find.where()
				.ilike("email", "%" + phrase + "%").findList();
		people.addAll(User.find.where().ilike("userName", "%" + phrase + "%")
				.findList());
		people.addAll(User.find.where().ilike("firstName", "%" + phrase + "%")
				.findList());
		people.addAll(User.find.where().ilike("lastName", "%" + phrase + "%")
				.findList());
		Set<User> s = new TreeSet<User>(new Comparator<User>() {

			@Override
			public int compare(User o1, User o2) {
				if (o1.id == o2.id)
					return 0;
				else
					return 1;
			}
		});
		s.remove(user);
		s.addAll(people);
		List<User> fin = Lists.newArrayList(s);
		return ok(searchPeople.render("People search", user, phrase, fin));
	}

	public static Result peopleSearch2(int tripId, String phrase) {
		User user = User.find.byId(Integer.valueOf(request().username()));
		List<User> people = User.find.where()
				.ilike("email", "%" + phrase + "%").findList();
		people.addAll(User.find.where().ilike("userName", "%" + phrase + "%")
				.findList());
		people.addAll(User.find.where().ilike("firstName", "%" + phrase + "%")
				.findList());
		people.addAll(User.find.where().ilike("lastName", "%" + phrase + "%")
				.findList());
		Set<User> s = new TreeSet<User>(new Comparator<User>() {

			@Override
			public int compare(User o1, User o2) {
				if (o1.id == o2.id)
					return 0;
				else
					return 1;
			}
		});
		s.remove(user);
		s.addAll(people);
		List<User> fin = Lists.newArrayList(s);
		return ok(tripPeopleSearch.render("Trip details", user, phrase, fin,
				Trip.find.byId(tripId)));
	}
}
