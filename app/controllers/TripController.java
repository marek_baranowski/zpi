package controllers;

import static play.data.Form.form;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.databind.node.ObjectNode;

import models.Activity;
import models.Destination;
import models.Trip;
import models.TripInvitation;
import models.User;
import play.data.Form;
import play.libs.Json;
import play.mvc.*;
import views.html.*;
import views.html.trip.*;

@Security.Authenticated(Secured.class)
public class TripController extends Controller {

	public static Result tripDetails(int tripId) {
		User user = User.find.byId(Integer.valueOf(request().username()));
		Trip trip = Trip.find.fetch("destinations")
				.fetch("destinations.activities").where().eq("id", tripId)
				.findUnique();
		Collections.sort(trip.destinations);
		for (Destination dest : trip.destinations) {
			Collections.sort(dest.activities);
		}
		return ok(timeline.render("Trip details", user, trip));
	}

	public static Result editTrip(int tripId) {
		User user = User.find.byId(Integer.valueOf(request().username()));
		return ok(editTrip.render("Trip details", user, Trip.find.byId(tripId),
				form(Trip.class)));
	}

	public static Result deleteTrip(int tripId) {
		Trip t = Trip.find.byId(tripId);
		for (Destination d : t.destinations) {
			for (Activity a : d.activities) {
				a.delete();
			}
			d.delete();
		}
		t.deleteManyToManyAssociations("users");
		t.deleteManyToManyAssociations("tags");
		t.delete();
		return redirect(routes.Application.myTrips());
	}

	public static Result updateTrip(int tripId) {
		User user = User.find.byId(Integer.valueOf(request().username()));
		Form<Trip> destForm = form(Trip.class).bindFromRequest();
		Trip old = Trip.find.byId(tripId);
		if (destForm.hasErrors()) {
			return badRequest(editTrip.render("Trip details", user, old,
					form(Trip.class)));
		} else {
			Trip trip = destForm.get();
			trip.id = old.id;
			trip.dateCreated = old.dateCreated;
			trip.users = old.users;
			trip.destinations = old.destinations;
			trip.update();
			return redirect(routes.TripController.tripDetails(trip.id));
		}
	}

	public static Result newDestinationView(int tripId) {
		User user = User.find.byId(Integer.valueOf(request().username()));
		return ok(newDestination.render("Trip details", user,
				Trip.find.byId(tripId), form(Destination.class)));
	}

	public static Result newDestination(int tripId) {
		User user = User.find.byId(Integer.valueOf(request().username()));
		Form<Destination> destForm = form(Destination.class).bindFromRequest();
		Trip trip = Trip.find.byId(tripId);
		if (destForm.hasErrors()) {
			return badRequest(newDestination.render("My trips", user, trip,
					destForm));
		} else {
			Destination dest = destForm.get();
			dest.dateCreated = new Timestamp(System.currentTimeMillis());
			dest.trip = trip;
			dest.save();
			return redirect(routes.TripController.tripDetails(tripId));
		}
	}

	public static Result editDestination(int destId) {
		User user = User.find.byId(Integer.valueOf(request().username()));
		Destination d = Destination.find.byId(destId);
		return ok(editDestination.render("Trip details", user, d,
				form(Destination.class)));
	}

	public static Result updateDestination(int destId) {
		User user = User.find.byId(Integer.valueOf(request().username()));
		Form<Destination> destForm = form(Destination.class).bindFromRequest();
		Destination old = Destination.find.byId(destId);
		if (destForm.hasErrors()) {
			return badRequest(editDestination.render("Trip details", user, old,
					form(Destination.class)));
		} else {
			Destination dest = destForm.get();
			dest.id = old.id;
			dest.dateCreated = old.dateCreated;
			dest.trip = old.trip;
			dest.activities = old.activities;
			dest.update();
			return redirect(routes.TripController.tripDetails(dest.trip.id));
		}
	}

	public static Result deleteDestination(int destId) {
		Destination d = Destination.find.byId(destId);
		d.delete();
		return redirect(routes.TripController.tripDetails(d.trip.id));
	}

	public static Result newActivity(int destId) {
		User user = User.find.byId(Integer.valueOf(request().username()));
		return ok(newActivity.render("Trip details", user,
				Destination.find.byId(destId), form(Activity.class)));
	}

	public static Result addActivity(int destId) {
		User user = User.find.byId(Integer.valueOf(request().username()));
		Form<Activity> activityForm = form(Activity.class).bindFromRequest();
		Destination dest = Destination.find.byId(destId);
		if (activityForm.hasErrors()) {
			return badRequest(newActivity.render("Trip details", user,
					Destination.find.byId(destId), form(Activity.class)));
		} else {
			Activity activity = activityForm.get();
			activity.dateCreated = new Timestamp(System.currentTimeMillis());
			activity.destination = dest;
			activity.save();
			return redirect(routes.TripController.tripDetails(dest.trip.id));
		}
	}

	public static Result editActivity(int activity) {
		User user = User.find.byId(Integer.valueOf(request().username()));
		Activity act = Activity.find.byId(activity);
		return ok(editActivity.render("Trip details", user, act.destination,
				act, form(Activity.class)));
	}

	public static Result updateActivity(int activityId) {
		User user = User.find.byId(Integer.valueOf(request().username()));
		Form<Activity> activityForm = form(Activity.class).bindFromRequest();
		Activity old = Activity.find.byId(activityId);
		Destination dest = old.destination;
		if (activityForm.hasErrors()) {
			return badRequest(editActivity.render("Trip details", user, dest,
					old, form(Activity.class)));
		} else {
			Activity activity = activityForm.get();
			activity.id = old.id;
			activity.dateCreated = old.dateCreated;
			activity.destination = dest;
			activity.update();
			return redirect(routes.TripController.tripDetails(dest.trip.id));
		}
	}

	public static Result deleteActivity(int activityId) {
		Activity a = Activity.find.byId(activityId);
		a.delete();
		return redirect(routes.TripController
				.tripDetails(a.destination.trip.id));
	}

	public static Result calendar(int tripId) {
		Trip t = Trip.find.byId(tripId);
		User user = User.find.byId(Integer.valueOf(request().username()));
		return ok(calendar.render("Trips", user, t));
	}

	public static Result tripPeople(int tripId) {
		User user = User.find.byId(Integer.valueOf(request().username()));
		return ok(tripPeople.render("Trip details", user,
				Trip.find.byId(tripId),
				TripInvitation.find.where().eq("trip.id", tripId).findList()));
	}

	public static Result inviteToTrip(int tripId, int userId) {
		User user = User.find.byId(Integer.valueOf(request().username()));
		User invited = User.find.byId(userId);
		TripInvitation ti = new TripInvitation();
		ti.sender = user;
		ti.receiver = invited;
		ti.trip = Trip.find.byId(tripId);
		ti.date = new Timestamp(System.currentTimeMillis());
		ti.save();
		if (Application.members.containsKey(user.id)) {
			ObjectNode event = Json.newObject();
			event.put("msg", "trip");
			Application.members.get(user.id).write(event);
		}
		return redirect(routes.TripController.tripPeople(tripId));
	}
	
	public static Result confirmInvitation(int tripId){
		User user = User.find.byId(Integer.valueOf(request().username()));
		Trip t = Trip.find.byId(tripId);
		t.users.add(user);
		t.update();
		TripInvitation.find.where().eq("receiver", user).eq("trip", t).findUnique().delete();
		return redirect(routes.TripController.tripPeople(tripId));
	}
	
	public static Result denyInvitation(int tripId){
		User user = User.find.byId(Integer.valueOf(request().username()));
		Trip t = Trip.find.byId(tripId);
		TripInvitation.find.where().eq("receiver", user).eq("trip", t).findUnique().delete();
		return redirect(routes.TripController.tripPeople(tripId));
	}
}
