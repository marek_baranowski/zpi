package controllers;

import static play.data.Form.form;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;

import models.Activity;
import models.Destination;
import models.Message;
import models.SearchedTrip;
import models.Tag;
import models.Trip;
import models.User;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.F.Callback0;
import play.libs.Json;
import play.mvc.*;
import views.html.*;
import views.html.trip.*;
import views.html.inbox.*;

@Security.Authenticated(Secured.class)
public class Application extends Controller {

	static Map<Integer, WebSocket.Out<JsonNode>> members = new HashMap<Integer, WebSocket.Out<JsonNode>>();

	public static WebSocket<JsonNode> pushService(final int userId) {
		return new WebSocket<JsonNode>() {
			// Called when the Websocket Handshake is done.
			public void onReady(WebSocket.In<JsonNode> in,
					WebSocket.Out<JsonNode> out) {
				// Join the chat room.
				members.put(userId, out);
			}
		};
	}

	public static Result myTrips() {
		User user = User.find.byId(Integer.valueOf(request().username()));
		return ok(myTrips.render("My trips", user, form(Trip.class),
				findUserActiveTrips(user.id), findUserPastTrips(user.id)));
	}

	public static List<Trip> findUserPastTrips(Integer userId) {
		return Trip.find
				.fetch("destinations")
				.where()
				.eq("users.id", userId)
				.lt("destinations.dateEnd",
						new Date(System.currentTimeMillis())).findList();
	}

	public static List<Trip> findUserActiveTrips(Integer userId) {
		return Trip.find
				.fetch("destinations")
				.where()
				.eq("users.id", userId)
				.ge("destinations.dateEnd",
						new Date(System.currentTimeMillis())).findList();
	}

	public static Result newTrip() {
		User logged = User.find.byId(Integer.valueOf(request().username()));
		Form<Trip> tripForm = form(Trip.class).bindFromRequest();
		if (tripForm.hasErrors()) {
			return badRequest(myTrips.render("My trips", logged, tripForm,
					new ArrayList<Trip>(), new ArrayList<Trip>()));
		} else {
			Trip trip = tripForm.get();
			User user = User.find.byId(Integer.valueOf(request().username()));
			trip.creator = user;
			trip.users.add(user);
			String[] tags = tripForm.data().get("tagsy").split(",");
			trip.tags = new LinkedList<Tag>();
			for (String tag : tags) {
				Tag t = Tag.find.where().eq("name", tag).findUnique();
				if (t == null) {
					t = new Tag();
					t.name = tag;
					t.save();
				}
				trip.tags.add(t);
			}
			trip.save();
			return redirect(routes.TripController.tripDetails(trip.id));
		}
	}

	public static Result inbox() {
		User user = User.find.byId(Integer.valueOf(request().username()));
		List<Message> messages = Message.find.where().eq("receiver", user)
				.orderBy("date desc").findList();
		return ok(inboxMain.render("Inbox", user, inbox.render(messages),
				"inbox"));
	}

	public static Result sent() {
		User user = User.find.byId(Integer.valueOf(request().username()));
		List<Message> messages = Message.find.where().eq("sender", user)
				.orderBy("date desc").findList();
		return ok(inboxMain
				.render("Inbox", user, sent.render(messages), "sent"));
	}

	public static Result showMessage(String flag, int messageId) {
		User user = User.find.byId(Integer.valueOf(request().username()));
		Message m = Message.find.byId(messageId);
		if (flag.equals("inbox") && m.status.equals("Unread")) {
			m.status = "Read";
			m.update();
		}
		return ok(inboxMain.render("Inbox", user,
				message.render(m, flag, form(Message.class)), flag));
	}

	public static Result trips() {
		List<Trip> tripy = Trip.find.all();
		User user = User.find.byId(Integer.valueOf(request().username()));
		return ok(trips.render("Trips", user, tripy));
	}

	public static Result tripsPost() {
		// get messages
		User user = User.find.byId(Integer.valueOf(request().username()));
		DynamicForm dynamicForm = form().bindFromRequest();
		String postDestTemp = dynamicForm.get("tagsy");
		String[] tagsy = postDestTemp.split(",");
		System.out.println("tripPost: " + tagsy);
		List<Trip> similar = findSimilar2(tagsy);
/*		String dateStrStart = dynamicForm.get("dateStart");
		Date dateStart = Date.valueOf(dateStrStart);
		String dateStrEnd = dynamicForm.get("dateEnd");
		Date dateEnd = Date.valueOf(dateStrEnd);*/
		return ok(trips.render("Trips", user, similar));
	}

	public static List<Trip> findSimilar2(String[] keywords) {
		List<Trip> tripy = Trip.find.all();
		List<SearchedTrip> st = new LinkedList<>();		
		for (String kw : keywords) {
			for (Trip t : tripy) {
				int eval = 0;
				String kwl = kw.toLowerCase();
				if (t.name.toLowerCase().contains(kwl)) {
					eval++;
				}
				if (t.description.toLowerCase().contains(kwl)) {
					eval++;
				}
				for (Destination d : t.destinations) {
					if (d.location.toLowerCase().contains(kwl)) {
						eval++;
					}
					for (Activity a : d.activities) {
						if (a.description.toLowerCase().contains(kwl)) {
							eval++;
						}
					}
				}
				if(eval > 0){
					st.add(new SearchedTrip(t, eval));
				}
			}
		}
		Collections.sort(st);
		List<Trip> output = new LinkedList<Trip>();
		for (SearchedTrip sss : st) {
			output.add(sss.trip);
		}
		return output;
	}

	public static List<Trip> findSimilar(String[] dests) {
		List<Trip> tripy = Trip.find.all();
		List<SearchedTrip> st = new LinkedList<>();
		for (Trip t : tripy) {
			SearchedTrip s = new SearchedTrip();
			s.trip = t;
			// s.setStringRepr();
			s.eval(dests);
			if (s.evaluation > 0) {
				st.add(s);
			}
		}
		Collections.sort(st);
		List<Trip> output = new LinkedList<Trip>();
		for (SearchedTrip sss : st) {
			output.add(sss.trip);
		}
		return output;
	}

	public static Result findMatches(int tripId) {
		Trip t = Trip.find.byId(tripId);
		User user = User.find.byId(Integer.valueOf(request().username()));
		String[] dests = new String[t.destinations.size()];
		for (int i = 0; i < dests.length; i++) {
			dests[i] = t.destinations.get(i).location;
		}
		List<Trip> output = findSimilar(dests);
		output.remove(t);
		return ok(matches.render("Trips", user, t, output));
	}

	public static Result fetch() {
		List<String> names = new ArrayList<String>();
		names.add("marek");
		names.add("pawe");
		return ok(Json.toJson(names));
	}

}
