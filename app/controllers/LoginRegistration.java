package controllers;

import static play.data.Form.form;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import models.Picture;
import models.User;
import play.*;
import play.data.Form;
import play.mvc.*;
import views.html.*;

public class LoginRegistration extends Controller {

	public static Result login() {
		return ok(login.render(form(Login.class), form(User.class)));
	}

	public static Result authenticate() {
		Form<Login> loginForm = form(Login.class).bindFromRequest();
		if (loginForm.hasErrors()) {
			return badRequest(login.render(loginForm, form(User.class)));
		} else {
			session().clear();
			User user = User.find.where().eq("email", loginForm.get().email)
					.findUnique();
			session("id", String.valueOf(user.id));
			return redirect(routes.Home.index());
		}
	}

	public static Result register() {
		Form<User> registerForm = form(User.class).bindFromRequest();
		if (registerForm.hasErrors()) {
			return badRequest(login.render(form(Login.class), registerForm));
		} else {
			User user = registerForm.get();
			user.profilePicture = Picture.find.byId(1);
			user.about = "";
			user.hometown = "";
			user.phone = "";
			user.dateRegistered = new Timestamp(System.currentTimeMillis());
			user.save();
			session("id", String.valueOf(user.id));
			return redirect(routes.Home.index());
		}
	}

	public static Result logout() {
		session().clear();
		return redirect(routes.LoginRegistration.login());
	}

}
