package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
public class Tag extends Model{

	@Id
	@GeneratedValue
	public int id;
	@Column
	public String name;
	
	public static Finder<Integer, Tag> find = new Finder<Integer, Tag>(
			Integer.class, Tag.class);
}
