package models;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import play.db.ebean.Model;

@Entity
public class User extends Model {

	@Id
	@GeneratedValue
	public int id;
	@Column
	public String firstName;
	@Column
	public String lastName;
	@Column
	public String userName;
	@Column
	public Date dateOfBirth;
	@Column
	public String gender;
	@Column
	public String hometown;
	@Column
	public String email;
	@Column
	public String password;
	@Column
	public String phone;
	@Column
	public String about;
	@Column
	public Timestamp dateRegistered;
	@OneToOne
	public Picture profilePicture;
	public List<User> friends;

	public static Finder<Integer, User> find = new Finder<Integer, User>(
			Integer.class, User.class);

	public String validate() {
		System.out.println(email);
		if (find.where().eq("email", email).findUnique() != null) {
			return "Email already used";
		}
		return null;
	}

	public static User authenticate(String email, String password) {
		return find.where().eq("email", email).eq("password", password)
				.findUnique();
	}
	
	public String friendshipStatus(User other){
		if(Friendship.find.where().eq("user1" , this).eq("user2" , other) != null){
			return "Friends";
		}
		else{
			if(Invitation.find.where().eq("sender", this) != null){
				return "Waiting";
			}
			else if(Invitation.find.where().eq("receiver", this) != null){
				return "Confirm";
			}
		}
		return "Not friends";
	}

}
