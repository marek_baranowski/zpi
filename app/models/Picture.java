package models;

import java.io.File;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import play.db.ebean.Model;
import play.db.ebean.Model.*;

@Entity
public class Picture extends Model {

	@Id
	@GeneratedValue
	public int id;
	@ManyToOne
	public User user;
	@Lob
	public byte[] picture;
	@Column
	public Timestamp dateUploaded;

	public static Finder<Integer, Picture> find = new Finder<Integer, Picture>(
			Integer.class, Picture.class);
}
