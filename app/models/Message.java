package models;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import play.db.ebean.Model;

@Entity
public class Message extends Model {

	@Id
	@GeneratedValue
	public int id;
	@ManyToOne
	public User sender;
	@ManyToOne
	public User receiver;
	@Column
	public Timestamp date;
	@Column
	public String message;
	@Column
	public String status;
	@Transient
	public int receiverId;

	public static Finder<Integer,Message> find = 
			new Model.Finder<>(Integer.class, Message.class);
	
}
