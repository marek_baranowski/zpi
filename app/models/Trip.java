package models;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import play.db.ebean.Model;

@Entity
public class Trip extends Model {

	@Id
	@GeneratedValue
	public int id;
	@Column
	public String name;
	@Column
	public String description;
	@Column
	public String visibility;
	@ManyToOne
	public User creator;
	@Column
	public Timestamp dateCreated;
	@ManyToMany
	public List<User> users;
	@OneToMany
	public List<Destination> destinations;
	@ManyToMany
	public List<Tag> tags; 

	public static Finder<Integer, Trip> find = new Finder<Integer, Trip>(
			Integer.class, Trip.class);

	@Override
	public String toString() {
		return "Trip [id=" + id + ", name=" + name + ", description="
				+ description + ", visiblity=" + visibility + "]";
	}
	
	public void getDestinations(){
		//Destination.find.setRawSql("")
	}
	
	public String getTags(){
		String tagi=tags.get(0).name;
		System.out.println(tags.size());
		for(int i = 1; i < tags.size(); i++){
			tagi += "," + tags.get(i).name;
		}
		System.out.println(tagi);
		return tagi;
	}

	public Date getEndDate() {
		Date latest = destinations.get(0).dateEnd;
		for (int i = 1; i < destinations.size(); i++) {
			if (latest.before(destinations.get(i).dateEnd)) {
				latest = destinations.get(i).dateEnd;
			}
		}
		return latest;
	}

	public long getDaysRemaining(Date startDate) {
		long mills_per_day = 1000 * 60 * 60 * 24;
		long day_diff = (startDate.getTime() - System.currentTimeMillis())
				/ mills_per_day;
		return day_diff;
	}

}
