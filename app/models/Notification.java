package models;

import java.sql.Timestamp;

public class Notification implements Comparable<Notification>{

	public String what;
	public User sender;
	public Timestamp date;
	public Trip trip_;
	@Override
	public int compareTo(Notification o) {
		// TODO Auto-generated method stub
		return date.compareTo(o.date);
	}
}
