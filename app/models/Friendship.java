package models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import play.db.ebean.Model;

@Entity
public class Friendship extends Model {

	@Id
	@GeneratedValue
	public int id;
	@ManyToOne
	public User user1;
	@ManyToOne
	public User user2;

	public static Finder<Integer, Friendship> find = new Finder<Integer, Friendship>(
			Integer.class, Friendship.class);

}
