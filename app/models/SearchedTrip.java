package models;

import java.util.LinkedList;
import java.util.List;

public class SearchedTrip implements Comparable<SearchedTrip> {

	public Trip trip;
	public List<String> destNames = new LinkedList<String>();
	public int evaluation;
	
	public SearchedTrip() {
		// TODO Auto-generated constructor stub
	}
	public SearchedTrip(Trip t, int e) {
		// TODO Auto-generated constructor stub
		trip = t;
		evaluation = e;
	}
	
	public void setStringRepr(){
		for(Destination dest: trip.destinations){
			destNames.add(dest.location);
		}
	}
	
	public int eval(String[] dests){
		for(String dest: dests){
			for(Destination d: trip.destinations){
				if(d.location.toLowerCase().contains(dest.toLowerCase())){
					evaluation++;
				}
			}
		}
		return evaluation;
	}

	@Override
	public int compareTo(SearchedTrip other) {
		// TODO Auto-generated method stub
		if(evaluation > other.evaluation)
			return -1;
		if(evaluation < other.evaluation)
			return 1;
		return 0;
	}
}
