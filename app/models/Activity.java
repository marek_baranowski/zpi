package models;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import play.db.ebean.Model;

@Entity
public class Activity extends Model implements Comparable<Activity> {

	@Id
	@GeneratedValue
	public int id;
	@Column
	public String description;
	@Column
	public String location;
	@Column
	public Date date;
	@Column
	public Timestamp dateCreated;
	@ManyToOne
	public Destination destination;

	public static Finder<Integer, Activity> find = new Finder<Integer, Activity>(
			Integer.class, Activity.class);

	@Override
	public int compareTo(Activity other) {
		// TODO Auto-generated method stub
		// first, sort by date (day)
		if (date.after(other.date)) {
			return 1;
		} else if (date.before(other.date)) {
			return -1;
		} else {
			return 0;
			/*// if activities are on the same day
			// if both have no time, pick later created
			if (time == null && other.time == null) {
				if (dateCreated.after(other.dateCreated)) {
					return 1;
				} else {
					return -1;
				}
			}
			// activity with time is more important
			else if (time == null) {
				return 1;
			} else if (other.time == null) {
				return -1;
			}
			// both activities have time
			else if (time.after(other.time)) {
				return 1;
			} else if (time.before(other.time)) {
				return -1;
			} else {
				// the same time, pick later created
				System.out.println("DUPA" + time + "/ " + other.time);
				if (dateCreated.after(other.dateCreated)) {
					return 1;
				} else {
					return -1;
				}
			}*/
		}
	}
}
