package models;

import java.util.HashMap;
import java.util.Map;

import play.libs.Akka;
import play.libs.Json;
import play.mvc.WebSocket;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import controllers.Application;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;

public class Push extends UntypedActor{

	static ActorRef push = Akka.system().actorOf(Props.create(Push.class));
	
	Map<Integer, WebSocket.Out<JsonNode>> members = new HashMap<Integer, WebSocket.Out<JsonNode>>();
	@Override
	public void onReceive(Object message) throws Exception {
		ObjectNode event = Json.newObject();
		event.put("msg", (String)message);
		//Application.members.get(receiver.id).write(event);
		
	}

}
