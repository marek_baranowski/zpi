package models;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import play.db.ebean.Model;

@Entity
public class Destination extends Model implements Comparable<Destination> {

	@Id
	@GeneratedValue
	public int id;
	@Column
	public String location;
	@Column
	public Date dateStart;
	@Column
	public Date dateEnd;
	@Column
	public Timestamp dateCreated;
	@OneToMany
	public List<Activity> activities;
	@ManyToOne
	public Trip trip;

	public static Finder<Integer, Destination> find = new Finder<Integer, Destination>(
			Integer.class, Destination.class);

	@Override
	public String toString() {
		return "Destination [id=" + id + ", location=" + location
				+ ", dateStart=" + dateStart + ", dateEnd=" + dateEnd
				+ ", trip=" + trip + "]";
	}

	public String validate() {
		return null;
	}

	@Override
	public int compareTo(Destination other) {
		// TODO Auto-generated method stub
		if (dateStart.after(other.dateStart)) {
			return 1;
		} else if (dateStart.before(other.dateStart)) {
			return -1;
		} else {
			if (dateCreated.after(other.dateCreated)) {
				return 1;
			} else {
				return -1;
			}
		}
	}
}
