package models;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import play.db.ebean.Model;

@Entity
public class Invitation extends Model {

	@Id
	@GeneratedValue
	public int id;
	@ManyToOne
	public User sender;
	@ManyToOne
	public User receiver;
	@Column
	public Timestamp date;

	public static Finder<Integer,Invitation> find = 
			new Model.Finder<>(Integer.class, Invitation.class);
	
}
