# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table activity (
  id                        integer auto_increment not null,
  description               varchar(255),
  location                  varchar(255),
  date                      date,
  date_created              datetime,
  destination_id            integer,
  constraint pk_activity primary key (id))
;

create table destination (
  id                        integer auto_increment not null,
  location                  varchar(255),
  date_start                date,
  date_end                  date,
  date_created              datetime,
  trip_id                   integer,
  constraint pk_destination primary key (id))
;

create table friendship (
  id                        integer auto_increment not null,
  user1_id                  integer,
  user2_id                  integer,
  constraint pk_friendship primary key (id))
;

create table invitation (
  id                        integer auto_increment not null,
  sender_id                 integer,
  receiver_id               integer,
  date                      datetime,
  constraint pk_invitation primary key (id))
;

create table message (
  id                        integer auto_increment not null,
  sender_id                 integer,
  receiver_id               integer,
  date                      datetime,
  message                   varchar(255),
  status                    varchar(255),
  constraint pk_message primary key (id))
;

create table picture (
  id                        integer auto_increment not null,
  user_id                   integer,
  picture                   longblob,
  date_uploaded             datetime,
  constraint pk_picture primary key (id))
;

create table tag (
  id                        integer auto_increment not null,
  name                      varchar(255),
  constraint pk_tag primary key (id))
;

create table trip (
  id                        integer auto_increment not null,
  name                      varchar(255),
  description               varchar(255),
  visibility                varchar(255),
  creator_id                integer,
  date_created              datetime,
  constraint pk_trip primary key (id))
;

create table trip_invitation (
  id                        integer auto_increment not null,
  sender_id                 integer,
  receiver_id               integer,
  trip_id                   integer,
  date                      datetime,
  constraint pk_trip_invitation primary key (id))
;

create table user (
  id                        integer auto_increment not null,
  first_name                varchar(255),
  last_name                 varchar(255),
  user_name                 varchar(255),
  date_of_birth             date,
  gender                    varchar(255),
  hometown                  varchar(255),
  email                     varchar(255),
  password                  varchar(255),
  phone                     varchar(255),
  about                     varchar(255),
  date_registered           datetime,
  profile_picture_id        integer,
  constraint pk_user primary key (id))
;


create table trip_user (
  trip_id                        integer not null,
  user_id                        integer not null,
  constraint pk_trip_user primary key (trip_id, user_id))
;

create table trip_tag (
  trip_id                        integer not null,
  tag_id                         integer not null,
  constraint pk_trip_tag primary key (trip_id, tag_id))
;
alter table activity add constraint fk_activity_destination_1 foreign key (destination_id) references destination (id) on delete restrict on update restrict;
create index ix_activity_destination_1 on activity (destination_id);
alter table destination add constraint fk_destination_trip_2 foreign key (trip_id) references trip (id) on delete restrict on update restrict;
create index ix_destination_trip_2 on destination (trip_id);
alter table friendship add constraint fk_friendship_user1_3 foreign key (user1_id) references user (id) on delete restrict on update restrict;
create index ix_friendship_user1_3 on friendship (user1_id);
alter table friendship add constraint fk_friendship_user2_4 foreign key (user2_id) references user (id) on delete restrict on update restrict;
create index ix_friendship_user2_4 on friendship (user2_id);
alter table invitation add constraint fk_invitation_sender_5 foreign key (sender_id) references user (id) on delete restrict on update restrict;
create index ix_invitation_sender_5 on invitation (sender_id);
alter table invitation add constraint fk_invitation_receiver_6 foreign key (receiver_id) references user (id) on delete restrict on update restrict;
create index ix_invitation_receiver_6 on invitation (receiver_id);
alter table message add constraint fk_message_sender_7 foreign key (sender_id) references user (id) on delete restrict on update restrict;
create index ix_message_sender_7 on message (sender_id);
alter table message add constraint fk_message_receiver_8 foreign key (receiver_id) references user (id) on delete restrict on update restrict;
create index ix_message_receiver_8 on message (receiver_id);
alter table picture add constraint fk_picture_user_9 foreign key (user_id) references user (id) on delete restrict on update restrict;
create index ix_picture_user_9 on picture (user_id);
alter table trip add constraint fk_trip_creator_10 foreign key (creator_id) references user (id) on delete restrict on update restrict;
create index ix_trip_creator_10 on trip (creator_id);
alter table trip_invitation add constraint fk_trip_invitation_sender_11 foreign key (sender_id) references user (id) on delete restrict on update restrict;
create index ix_trip_invitation_sender_11 on trip_invitation (sender_id);
alter table trip_invitation add constraint fk_trip_invitation_receiver_12 foreign key (receiver_id) references user (id) on delete restrict on update restrict;
create index ix_trip_invitation_receiver_12 on trip_invitation (receiver_id);
alter table trip_invitation add constraint fk_trip_invitation_trip_13 foreign key (trip_id) references trip (id) on delete restrict on update restrict;
create index ix_trip_invitation_trip_13 on trip_invitation (trip_id);
alter table user add constraint fk_user_profilePicture_14 foreign key (profile_picture_id) references picture (id) on delete restrict on update restrict;
create index ix_user_profilePicture_14 on user (profile_picture_id);



alter table trip_user add constraint fk_trip_user_trip_01 foreign key (trip_id) references trip (id) on delete restrict on update restrict;

alter table trip_user add constraint fk_trip_user_user_02 foreign key (user_id) references user (id) on delete restrict on update restrict;

alter table trip_tag add constraint fk_trip_tag_trip_01 foreign key (trip_id) references trip (id) on delete restrict on update restrict;

alter table trip_tag add constraint fk_trip_tag_tag_02 foreign key (tag_id) references tag (id) on delete restrict on update restrict;

# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table activity;

drop table destination;

drop table friendship;

drop table invitation;

drop table message;

drop table picture;

drop table tag;

drop table trip;

drop table trip_user;

drop table trip_tag;

drop table trip_invitation;

drop table user;

SET FOREIGN_KEY_CHECKS=1;

